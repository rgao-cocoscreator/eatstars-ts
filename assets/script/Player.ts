const { ccclass, property } = cc._decorator

@ccclass
export class Player extends cc.Component {
    //主角跳跃高度
    @property(cc.Integer)
    private jumpHeight: number = 0;
    //主角跳跃持续时间
    @property(cc.Integer)
    private jumpDuration: number = 0;
    //最大移动速度
    @property(cc.Integer)
    private maxMoveSpeed: number = 0;
    //加速度
    @property(cc.Integer)
    private accel: number = 0;
    //跳跃音效资源
    @property({type:cc.AudioClip})
    jumpAudio: cc.AudioClip = null;

    private jumpAction: cc.Action = null;
    private xSpeed: number = 0;
    private accLeft: boolean = false;
    private accRight: boolean = false;

    protected onLoad() {
        //初始化跳跃动作
        this.jumpAction = this.setJumpAction();
        this.node.runAction(this.jumpAction);

        //加速度方向开关
        this.accLeft = false;
        this.accRight = false;
        //主角当前水平方向速度
        this.xSpeed = 0;

        //初始化键盘监听输入监听
        this.addEventListeners();
    }

    private setJumpAction() {
        //跳跃上升
        let jumpUp = cc.moveBy(this.jumpDuration, cc.v2(0, this.jumpHeight)).easing(cc.easeCubicActionOut());
        //下落
        let jumpDown = cc.moveBy(this.jumpDuration, cc.v2(0, -this.jumpHeight)).easing(cc.easeCubicActionIn());
        //添加一个回调函数，用于在动作结束时调用我们定义的其他方法
        let callback = cc.callFunc(this.playJumpSound, this);
        //不同重复
        return cc.repeatForever(cc.sequence(jumpUp, jumpDown, callback));

    }

    playJumpSound() {
        //调用声音引擎播放声音
        cc.audioEngine.playEffect(this.jumpAudio as any, false);
    }

    private addEventListeners() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        // cc.find("Canvas").on(cc.Node.EventType.TOUCH_START,this.onScreenTouchStart,this);
    }
    private onKeyDown(event: cc.Event.EventKeyboard) {
        //set a flag when key pressed
        switch (event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.moveLeft();
                break;
            case cc.macro.KEY.d:
            case cc.macro.KEY.right:
                this.moveRight();
                break;
        }
    }

    private onKeyUp(event: cc.Event.EventKeyboard) {
        switch (event.keyCode) {
            case cc.macro.KEY.a:
            case cc.macro.KEY.left:
                this.stopMove();
                break;
            case cc.macro.KEY.b:
            case cc.macro.KEY.right:
                this.stopMove();
                break;
        }
    }

    private moveLeft() {
        this.accLeft = true;
        this.accRight = false;
    }

    private moveRight() {
        this.accRight = true;
        this.accLeft = false;
    }

    private stopMove() {
        this.accLeft = false;
        this.accRight = false;
    }

    onDestroy() {
        //取消键盘输入监听
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    protected update(dt: number) {
        if (this.accLeft) {
            this.xSpeed -= this.accel * dt;
        } else if (this.accRight) {
            this.xSpeed += this.accel * dt;
        }
        //限制主角的速度不能超过最大值
        if (Math.abs(this.xSpeed) > this.maxMoveSpeed) {
            this.xSpeed = this.maxMoveSpeed * this.xSpeed / Math.abs(this.xSpeed);
        }

        //根据当前速度更新主角的位置
        this.node.x += this.xSpeed * dt;
    }

}